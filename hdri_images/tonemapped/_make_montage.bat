@echo off

echo Note: There should be an even number of images

rm _montage.jpg

echo Montage...
montage *.jpg -tile x2 -geometry +0+0 montage_tmp.png

echo JPG...
convertimg montage_tmp.png -resize x340 -quality 85 -strip -interlace Plane montage_tmp.jpg

echo Crush...
jpegtran -copy none -optimize -progressive montage_tmp.jpg _montage.jpg

rm montage_tmp.jpg
rm montage_tmp.png

echo Done!
sleep 0.5